.PHONE: up down install_argocd install_applicationset install_apps

up:
	kind create cluster --name api-local --config=kind-config.yaml

down:
	kind delete cluster --name api-local

install_argocd:
	helm repo add argo https://argoproj.github.io/argo-helm && \
	kubectl create ns argocd && \
	kubens argocd && \
	helm install --version 3.0.0 argocd argo/argo-cd

install_applicationset:
	kubens argocd && \
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/HEAD/manifests/base/config/argocd-gpg-keys-cm.yaml && \
	kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/applicationset/master/manifests/install.yaml

install_apps:
	kubectl apply -f applications/applications.yaml

# ls resources/*/argocd/applications.yaml